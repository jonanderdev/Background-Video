
# .....	GLOBAL ......
window.bgVideo = {}
 
#Add the script needed for the YTObjects to work
addScriptYTObjects = ->
  script = document.createElement('script')
  script.src = "http://ajax.googleapis.com/ajax/libs/swfobject/2.2/swfobject.js"
  script.type = 'text/javascript'
  head = document.getElementsByTagName('head')[0]
  head.appendChild(script)
#Call the function to add the script
addScriptYTObjects()

addEvent = (elem, type, eventHandle) ->
  if not elem? 
  	return
  if elem.addEventListener 
    elem.addEventListener type, eventHandle, false 
  else if elem.attachEvent
    elem.attachEvent "on#{type}", eventHandle
  else
    elem["on#{type}"] = eventHandle
#End addEvent


#Center the video vertically
centerBgVideos = ->
  videoContainers = []
  for video in bgVideo.videos
    videoContainers.push document.getElementById(video.id) 
  for videoContainer in videoContainers
    centerWrapper = videoContainer.children[0]
    newDimensions = getDimensions(videoContainer, centerWrapper)
    if videoContainer.offsetWidth < 1024 
      centerWrapper.style.minWidth = '0'
      centerWrapper.style.width = "#{newDimensions.width}px"
      centerWrapper.style.height = "#{newDimensions.height}px"
      centerWrapper.style.left = '50%'
      centerWrapper.style.marginLeft = "-" + newDimensions.width/2 + "px"
    else
      centerWrapper.style.minWidth = '1024px'
      centerWrapper.style.width = '100%'
      centerWrapper.style.height = 'auto'
      centerWrapper.style.left = 'auto'
      centerWrapper.style.marginLeft = '0'
    centerWrapper.style.marginTop = '-' + ( centerWrapper.offsetHeight - videoContainer.offsetHeight ) / 2 + 'px'
#End centerBgVideos


#Get the dimensions of the video
getDimensions = (container, wrapper) ->
  containerHeight = container.offsetHeight
  containerWidth = container.offsetWidth
  wrapperHeight = wrapper.offsetHeight
  wrapperWidth = wrapper.offsetWidth  
  if containerWidth >= containerHeight
    wrapperWidth = containerWidth
    if getValue16x9('height', wrapperWidth) > containerHeight 
      containerHeight = getValue16x9('height', wrapperWidth)
    else
      wrapperHeight = containerHeight
      wrapperWidth = getValue16x9('width', wrapperHeight)
  else
     wrapperHeight = containerHeight
    if getValue16x9('width', wrapperHeight) > containerWidth 
      containerWidth = getValue16x9('width', wrapperHeight) 
    else
      wrapperWidth = containerWidth
      wrapperHeight = getValue16x9('height', wrapperWidth)
  return { width: wrapperWidth, height: wrapperHeight}
#End getDimensions
    
getValue16x9 = (heightOrWidth, knownValue) ->
  if heightOrWidth is 'width'
  	return (knownValue * 16) / 9
  if heightOrWidth is 'height'
    return (knownValue * 9) / 16
  return -1
#End getValue16x9


class BgVideo
  constructor: (@parentId, @id, @youtubeId, @mustBeMuted, @mustBeRepeated, @callback) ->
    muteApplied = false
    parentBackground = 'none'
    callbackCalled = false

  #Instance Methods
  getPlayer: ->
    document.getElementById("#{@id}-player")
  play: ->
    @.getPlayer().playVideo()
  pause: ->
    @.getPlayer().pauseVideo()
  isPlaying: ->
    if @.getPlayer().getPlayerState() is 1 then return true  else return false
  mute: ->
    @.getPlayer().mute()
  unMute: ->
    @.getPlayer().unMute()
  isMuted: ->
    @.getPlayer().isMuted()
  replace: (newYTVideoId) ->
  	@.getPlayer().loadVideoById(newYTVideoId)
  destroy: ->
    elem = document.getElementById("#{@id}")
    elem.parentNode.removeChild(elem)
    idNum = @id.replace( /^\D+/g, '');
    bgVideo.videos.splice [(idNum-1) , 1]
#End Class BgVideo



#Start init --------------
bgVideo.init = ->	
  
  #Add css style for the videos
  addCss()

  #Create the array of videos
  bgVideo.videos = []
  
  window.onYouTubePlayerReady = (playerid) ->
    player = document.getElementById(playerid)
    player.addEventListener("onStateChange", "updateShownVideos")
  #End onYouTubePlayerReady

  window.updateShownVideos = (newState) ->
    for video in bgVideo.videos    	
      if video.getPlayer().getPlayerState() is 1 #The video has started      
        document.getElementById(video.parentId).style.background = 'none'
        video.getPlayer().style.opacity = '1'
        if not video.callbackCalled
          video.callback?() 
          video.callbackCalled = true
        if video.mustBeMuted and not video.muteApplied 
          video.mute()
          video.muteApplied = true
      else if video.getPlayer().getPlayerState() is 0 #The video has finished. If the video is in loop mode we play it again
        if video.mustBeRepeated is 1 then video.play()
    return
  #End updateShownVideos
#End init ---------------


#Add css style for the videos
addCss = ->
  css = document.createElement("style")
  css.type = "text/css"
  css.innerHTML = "
	.videoContainer{
	  position: absolute; left: 0;top: 0;
	  overflow: hidden;
	  margin: 0;padding: 0;
	  height: 100%; width: 100%;
	  z-index: -999998;
	}
	.centerWrapper{
	  /* Set rules to fill background */
	  min-height: 100%; min-width: 1024px;
	  /* Set up proportionate scaling */
	  position: absolute;
	  width: 100%;height: auto;
	}
	.videoWrapper {
	  position: relative;
	  padding-bottom: 56.25%; padding-top:0;
	  height:0;overflow:hidden;
	}
	.videoWrapper div,.videoWrapper embed,.videoWrapper object{
	  position:absolute; top:0;left:0;
	  width: 100%;height:100%;
	  opacity: 0;
	  transition: opacity .25s ease; -webkit-transition: opacity .25s ease; -moz-transition: opacity .25s ease;-o-transition: opacity .25s ease;
	}"
  document.body.appendChild(css)
#End AddCss --------------------




#Add the html for the container of the video
addVideoContainer = (containerId, videoId) ->
  container = document.getElementById(containerId)
  container.innerHTML += 
  	"<div id='#{videoId}' class='videoContainer'>
  	<div class='centerWrapper'>
    <div class='videoWrapper'>
    <div id='#{videoId}-player'>
    </div></div></div></div>"
#End addVideoContainer





#Add video------------------------
bgVideo.addVideo = (options) -> #containerId, youtubeId, parameters ...
  containerId = options.parentId
  youtubeId = options.videoId

  #Get the id of the new video
  numVideo = bgVideo.videos.length + 1
  videoId = "video-#{numVideo}"
  
  #First we create the container for the video
  addVideoContainer(containerId, videoId)
  
  #Set the parameters for the video
  finalParameters =  
    autoplay: options.autoplay ? '1'
    repeat: options.loop ? '1'
    mute:  options.mute ? true
    start: options.start ? '0'
    end: options.end 

  if finalParameters.repeat is '1' then repeat = "&loop=1&playlist=#{youtubeId}" else repeat = ""
  if finalParameters.end? then end = "&end=#{finalParameters.end}" else ""

  #Youtube video parameters 
  #For more info go to https://developers.google.com/youtube/player_parameters
  videoParams = 
    "controls=0&" +
    "html5=1&" +
    "iv_load_policy=3&" +
    "wmode=transparent&" +
    "showinfo=0&" +
    "start=#{finalParameters.start}&" +
    "modestbranding=1&" +
    "disablekb=1&" +
    "rel=0&" +
    "vq=hd720&" +
    "autoplay=#{finalParameters.autoplay}" +
    "#{repeat}" +
    "#{end}"
  
  #Other parameters
  params =
    allowScriptAccess: "always"
  atts =
    id: "#{videoId}-player"

  swfobject.embedSWF(
    "http://www.youtube.com/v/#{youtubeId}?enablejsapi=1&playerapiid=#{videoId}-player&version=3&#{videoParams}"
    "#{videoId}-player"
    "1280"
    "720"
    "8"
    null
    null
    params
    atts
  )
  
  #Create the video and put it in the list of videos
  bgVideo.videos.push new BgVideo(options.parentId, videoId,youtubeId, finalParameters.mute, finalParameters.repeat, options.callback)
  	
  #Center the video and the others
  centerBgVideos()

  return bgVideo.videos[bgVideo.videos.length-1]
#End addVideo------------------------



#Add the events to the window closure
addEvent(window, 'resize', centerBgVideos)
addEvent(window, 'load', bgVideo.init)



