window.onload = function(){	
//BgVideo Pluging
			/*  Example of use -  Adding a background video to #home element  */
			var video = bgVideo.addVideo({
			    parentId: "home",
			    videoId: "n0g9K_C2C2c",
			    callback: onVideoReady
			});

			$("#muteUnMute").click(function(){
				if(video.isMuted()){
			/* -----> Example of use - Remove the silence of the video*/											
					video.unMute();

					$("#muteUnMute i").attr('class', 'fa fa-volume-up');//Change the icon of the method
					$("#muteUnMute span").html("mute()");//Change the text of the method
				}else{
			/* ----> Example of use  - Silence the video*/
					video.mute();

					$("#muteUnMute i").attr('class', 'fa fa-volume-off');//Change the icon of the method
					$("#muteUnMute span").html("unMute()");//Change the text of the method
				}
			});

			$("#playPause").click(function(){
				if(video.isPlaying()){
			/* -----> Example of use - Remove the silence of the video*/											
					video.pause();

					$("#playPause i").attr('class', 'fa fa-play');//Change the icon of the method
					$("#playPause span").html("play()");//Change the text of the method
				}else{
			/* ----> Example of use  - Silence the video*/
					video.play();

					$("#playPause i").attr('class', 'fa fa-pause');//Change the icon of the method
					$("#playPause span").html("pause()");//Change the text of the method
				}
			});

			$("#replaceIcon").click(function(){
				if($("#replaceInput").val()){
					var newVideoId = $("#replaceInput").val();
			/* ----> Example of use  - Replace the video*/
					video.replace(newVideoId);
				}
			});
			$("#replaceInput").keypress(function(e) {
			    if($("#replaceInput").val()){
					var newVideoId = $("#replaceInput").val();
			/* ----> Example of use  - Replace the video*/
					video.replace(newVideoId);
				}
			});

			$("#replace form").submit(function(e) {
    			e.preventDefault();
			});

			/* ----> Example of use  - This function is called when the video is ready */
			function onVideoReady(){
				showMethods();				
			}

			function showMethods(){
				$("#loading").remove();
				$(".methods").fadeIn("slow");
			}

		}