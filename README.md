# BgVideo
Youtube Video Background plugin in progress

Example: http://jonandev.com/web/BgVideo/

Simple plugin to use Youtube videos as a Background using youtube api

No jquery dependency

## Easy to use
Add the script on the bottom of your page
```html
	<!-- BgVideo Script -->
	<script src="src/bgVideo.js"></script>
```

Adding a background video to a element.
```javascript

	/*  Example of use -  Adding a background video to the #home element  */
	var video = bgVideo.addVideo({
	  parentId: "home",
	  videoId: "n0g9K_C2C2c"
	});
```
This has to be done after the page is loaded.
The element provided has to have position: relative;



## Options
```javascript
	/*  Example of use -  Adding a background video to the #home element  */
	var video = bgVideo.addVideo({
	  parentId: "home",
	  videoId: "n0g9K_C2C2c",
	  //Other options with the default values
	  autoplay: true, //To start playing the video automatically
	  mute: true,  //To mute the video
	  loop: true, //To repeat the video
	  start: 0,  //The second to start the video
	  end: //Set the time to end the video (in seconds). Default value is the video actual lenght. 
	});
```

## Methods
```javascript
/* Methods */
	//Play the video
	video.play(); 
	
	//Is it playing?
	video.isPlaying();
	
	//Pause the video
	video.pause(); 
	
	//Silence the video
	video.mute(); 
	
	//Boolean: Is the video muted?
	video.isMuted(); 
	
	//Remove the silence of the video
	video.unMute(); 
	
	//To change the video pass the id of the new one to this function
	video.replace(newYoutubeVideoId);
	
	//To destroy the video element
	video.destroy();
	
	//To get the player and therefore all the methods that the player of Youtube javascript API provides
	//See all the methods in the next section
	video.getPlayer();
```

## Other Methods
It uses Youtube javascript API  so you can use all the methods there by doing video.getPlayer()

[Youtube javascript API](https://developers.google.com/youtube/js_api_reference)

```javascript
  //Example
  video.getPlayer().playVideo();
```

There is a problem in firefox with the z-index and negative values so maybe it doesn't work in the old firefox versions.
